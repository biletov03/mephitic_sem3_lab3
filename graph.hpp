#ifndef UNTITLED1_GRAPH_HPP
#define UNTITLED1_GRAPH_HPP
#include <iostream>
#include <fstream>
#include <queue>
#include <vector>
#include <map>
//#include "Path.hpp"

using namespace std;

template <typename TWeight>
class Path;

class Node {
private:
    vector<int> node;
public:
    Node():node() {}

    explicit Node(size_t n):node(n) {}

    int get_element(int ind)
    {
        return node[ind];
    }

    size_t get_size()
    {
        return node.size();
    }

    void append(int element)
    {
        node.push_back(element);
    }

    void erase_id(int _id)
    {
        auto it = node.begin();

        while (*it != _id) it++;

        node.erase(it);
    }
};

template <typename TWeight>
class Edge {
private:
    int start;
    int end;
    TWeight weight;
public:
    Edge():start(), end(), weight() {}

    Edge(int _start, int _end, TWeight _weight)
    {
        start = _start;
        end = _end;
        weight = _weight;
    }

    int get_end()
    {
        return end;
    }
    int get_start()
    {
        return start;
    }
    TWeight get_weight()
    {
        return weight;
    }

    void add_end(int element)
    {
        end = element;
    }

    void add_start(int element)
    {
        start = element;
    }

    void add_weight(TWeight element)
    {
        weight = element;
    }
};

template <typename TWeight>
class Graph {
private:
    inline static int id = 1;
    map<int, Edge<TWeight>> edges;
    vector<Node> nodes;

public:

    Graph(): nodes(), edges() {}

    int get_end(int ind)
    {
        return edges[ind].get_end();
    }
    int get_start(int ind)
    {
        return edges[ind].get_start();
    }
    TWeight get_weight(int ind)
    {
        return edges[ind].get_weight();
    }

    size_t get_size_node()
    {
        return nodes.size();
    }

    void add_node()
    {

        nodes.emplace_back();
    }

    void add_nodes(size_t count)
    {
        for (int i = 0; i < count; ++i)
        {
            nodes.emplace_back();
        }
    }

    void add_edge(int _start, int _end, TWeight _weight)
    {
        Edge edge(_start, _end, _weight);
        (nodes[_start]).append(id);
        edges[id++] = edge;
    }

    void remove_edge(int _id)
    {
        if (_id >= id)
        {
            return;
        }

        nodes[edges[_id].get_start()].erase_id(_id);
        edges.erase(_id);
    }

    int find_edge(int _start, int _end)
    {
        for (int i = 1; i < id; ++i)
        {
            if (edges[i].get_start() == _start and edges[i].get_end() == _end)
            {
                return i;
            }
        }
        return 0;
    }
    Path<TWeight> * Dijkstra(int _start, int _end)
    {
        queue<Node> que;
        que.push(nodes[_start]);
        vector<TWeight> d(nodes.size(), INT_MAX);
        d[_start] = 0;
        vector<int> p(nodes.size(), -2);
        p[_start] = -1;
        while (!que.empty())
        {
            Node v = que.front();
            que.pop();
            for (int i = 0; i < v.get_size(); ++i) // auto arc_id: v.node
            {
                auto edge = edges[v.get_element(i)];
                if (d[edge.get_start()] + edge.get_weight() < d[edge.get_end()])
                {
                    d[edge.get_end()] = d[edge.get_start()] + edge.get_weight();
                    p[edge.get_end()] = v.get_element(i);
                    que.push(nodes[edge.get_end()]);
                }
            }
        }
        vector<int> res;
        auto id_s = p[_end];
        res.push_back(p[_end]);
        while(edges[id_s].get_start() != _start)
        {
            res.push_back(p[edges[id_s].get_start()]);
            id_s = p[edges[id_s].get_start()];
        }
        res.push_back(d[_end]);
        res.erase(--(res.end()));
        reverse(res.begin(), res.end());
        auto path = new Path<TWeight>(this, res);
        return path;
    }

    void Graphviz()
    {
        if (nodes.empty()) return;

        std::ofstream out;
        out.open("graph.gv");
        out << "digraph Gr {\nrankdir=LR;\nnode [shape = circle];\n";
        for (int i = 0; i < nodes.size(); ++i)
        {
            out << i << "\n";
            for (int j = 0; j < nodes[i].get_size(); ++j)
            {
                out << i << " -> " << edges[nodes[i].get_element(j)].get_end() << " [label = \"" << edges[nodes[i].get_element(j)].get_weight() << "\"];\n";
            }
        }
        out << "}";
        out.close();
        system(((string)"dot -Tpng graph.gv -o Graph.png").c_str());
        system(((string)"rm graph.gv").c_str());
    }

    void Dfs_TS(int &pos, int i, vector<int> &ans, vector<bool> &used)
    {
        used[i] = true;
        for (int j= 0; i < nodes[j].get_size(); ++j) //auto arc_id: nodes[i].node
        {
            if (used[edges[nodes[i].get_element(j)].get_end()])
            {
                continue;
            }
            Dfs_TS(pos, edges[nodes[i].get_element(j)].get_end(), ans, used);
        }
        ans[pos] = i;
        pos -= 1;
    }

    vector<int> topological_sort()
    {
        vector<int> ans(nodes.size(), -1);
        vector<bool> used(nodes.size(), false);
        int pos;
        pos = nodes.size() - 1;
        for (int i = 0; i < (int)nodes.size(); i++)
        {
            if (!used[i])
            {
                Dfs_TS(pos, i, ans, used);
            }
        }
        return ans;
    }

    Graph<TWeight> spanning_tree()
    {
        Graph<TWeight> res;
        res.add_nodes(nodes.size());
        vector<bool> used(nodes.size(), false);
        DfsST(3, used, res);
        return res;
    }

    void DfsST(int v, vector<bool> &used, Graph<TWeight> &res)
    {
        used[v] = true;
        for (int i = 0; i < nodes[v].get_size(); ++i)
        {
            auto edge = edges[nodes[v].get_element(i)];
            if (used[edge.get_end()])
            {
                continue;
            }
            res.add_edge(edge.get_start(), edge.get_end(), edge.get_weight());
            DfsST(edge.get_end(), used,  res);
        }
    }

    Graph<TWeight> reversed_graph()
    {
        Graph<TWeight> graph_result;
        graph_result.add_nodes(nodes.size());
        for (int i = 0; i < graph_result.nodes.size(); ++i)
        {
            for (int k = 0; k < nodes[i].get_size(); ++k)
            {
                int start, end;
                start = edges[nodes[i].get_element(k)].get_end();
                end = edges[nodes[i].get_element(k)].get_start();
                TWeight weight = edges[nodes[i].get_element(k)].get_weight();
                graph_result.add_edge(start, end, weight);
            }
        }
        return graph_result;
    }

    void Dfs_scc(vector<bool> &used, int pos)
    {
        used[pos] = true;
        for (int i = 0; i < nodes[pos].get_size(); ++i)
        {
            if (used[edges[nodes[pos].get_element(i)].get_end()] == false)
            {
                Dfs_scc(used, edges[nodes[pos].get_element(i)].get_end());
            }
        }
    }

    vector<vector<int>> strong_con_com()
    {
        Graph<TWeight> graph_rev = reversed_graph();
        vector<vector<int>> scc;
        vector<int> help;

        for (int i = 0; i < nodes.size(); ++i)
        {
            help.push_back(i);
        }
        for (int i = 0; i < nodes.size(); ++i)
        {
            if (help[i] != -1) {
                vector<int> scc_2;
                vector<bool> used_start(nodes.size(), false);
                vector<bool> used_reversed(nodes.size(), false);
                int pos1 = i, pos2 = i;
                Dfs_scc(used_start, pos1);
                graph_rev.Dfs_scc(used_reversed, pos2);
                for (int j = 0; j < used_start.size(); ++j)
                {
                    if (used_start[j] == 1 and used_reversed[j] == 1)
                    {
                        help[j] = -1;
                        scc_2.push_back(j);
                    }
                }
                scc.push_back(scc_2);
            }
        }
        return scc;
    }
};

template <typename TWeight>
class Path {
private:
    Graph<TWeight> * graph;
    vector<int> id_s;
    vector<int> nodes;
    TWeight weight;
public:
    Path():id_s(), nodes() {}

    explicit Path(Graph<TWeight> * graph_p): graph(graph_p), id_s(), nodes() {}

    Path(Graph<TWeight> * graph_p, const vector<int>& path)
    {
        graph = graph_p;
        id_s = path;
        if (!path.empty())
        {
            nodes.push_back(graph->get_start(id_s[0]));
            nodes.push_back(graph->get_end(id_s[0]));
            weight += graph->get_weight(id_s[0]);
        }
        for (int i = 1; i < id_s.size(); ++i)
        {
            nodes.push_back(graph->get_end(id_s[i]));
            weight += graph->get_weight(id_s[i]);
        }
    }

    vector<int> * get_path_nodes()
    {
        auto * res = new vector<int>(nodes);
        return res;
    }
    vector<int> * get_path_ids()
    {
        auto * res = new vector<int>(id_s);
        return res;
    }

    TWeight get_path_weight()
    {
        return weight;
    }

    void set_graph(Graph<TWeight> * graph_p)
    {
        graph = graph_p;
    }

    Path * crossing(Path<TWeight> * path_2)
    {
        auto res = new Path<TWeight>();
        if (graph != path_2->graph)
        {
            return res;
        }
        res->set_graph(graph);
        int i = 0;
        while (i < id_s.size()) {
            int j = 0;
            while (j < path_2->id_s.size()) {
                if (id_s[i] == path_2->id_s[j]) {
                    res->id_s.push_back(id_s[i]);
                    ++j;
                    ++i;
                } else {
                    ++j;
                }
            }
            ++i;
        }
        if (res->id_s.empty())
        {
            return res;
        } else {
            res->nodes.push_back(res->graph->get_start(res->id_s[0]));
            res->nodes.push_back(res->graph->get_end(res->id_s[0]));
            res->weight += res->graph->get_weight(res->id_s[0]);
        }
        for (int j = 1; j < res->id_s.size(); ++j)
        {
            res->nodes.push_back(res->graph->get_end(res->id_s[j]));
            res->weight += res->graph->get_weight(res->id_s[j]);
        }
        return res;
    }

    Path * unite(Path<TWeight> * path_2)
    {
        auto res = new Path<TWeight>();
        if (graph != path_2->graph)
        {
            return res;
        }
        res->set_graph(graph);
        int flag = 0;
        int fix_i;
        for (int i = 0; i < nodes.size(); ++i)
        {
            for (int j = 0; j < path_2->nodes.size(); ++j)
            {
                if (nodes[i] == path_2->nodes[j])
                {
                    flag = 1;
                    fix_i = i;
                    break;
                }
            }
            if (flag) {
                break;
            }
        }

        if (!flag)
        {
            return res;
        }
        if (fix_i == 0) {
            for (int j = 0; j < path_2->nodes.size(); ++j)
            {
                res->nodes.push_back(path_2->nodes[j]);
            }
            flag = 0;
            for (int & node : nodes)
            {
                if (flag)
                {
                    res->nodes.push_back(node);
                }
                if (node == path_2->nodes[path_2->nodes.size() - 1])
                {
                    flag = 1;
                }
            }
        } else {
            for (int & node : nodes)
            {
                res->nodes.push_back(node);
            }
            flag = 0;
            for (int j = 0; j < path_2->nodes.size(); ++j)
            {
                if (flag)
                {
                    res->nodes.push_back(path_2->nodes[j]);
                }
                if (path_2->nodes[j] == nodes[nodes.size() - 1])
                {
                    flag = 1;
                }
            }
        }
        for (int i = 0; i < res->nodes.size() - 1; ++i)
        {
            int edge_id = graph->find_edge(res->nodes[i], res->nodes[i + 1]);
            res->id_s.push_back(edge_id);
            res->weight += graph->get_weight(edge_id);
        }
        return res;
    }
};

#endif //UNTITLED1_GRAPH_HPP
