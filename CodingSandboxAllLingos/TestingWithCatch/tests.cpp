#include <string>
#include "catch.hpp"
#include "../../../mephi_sem2_lab2/array_sequence.hpp"
#include "../../graph.hpp"

using namespace std;

TEST_CASE("Sequence test", "[Sequence]")
{
   SECTION("Append")
   {
      Sequence<int> *arr = new ArraySequence<int>();
      for (int i = 0; i < 50; ++i)
      {
          arr->Append(i);
      }

      REQUIRE(arr->GetLength() == 50);

      for (int i = 0; i < 50; ++i)
      {
          REQUIRE(arr->Get(i) == i);
      }
   }

    SECTION("Prepend")
    {
       Sequence<int> * arr = new ArraySequence<int>();
       for (int i = 0; i < 50; ++i)
       {
           arr->Prepend(i);
       }

       REQUIRE(arr->GetLength() == 50);

       for (int i = 0; i < 50; ++i)
       {
           REQUIRE(arr->Get(49 - i) == i);
       }
   }

    SECTION("Copy")
    {
        Sequence<int> *arr = new ArraySequence<int>();
        for (int i = 0; i < 50; ++i)
        {
            arr->Append(i);
        }
        auto arr_copy = arr->Copy();

        REQUIRE(arr_copy->GetLength() == 50);
        for (int i = 0; i < 50; ++i)
        {
            REQUIRE(arr_copy->Get(i) == i);
        }
   }
}

TEST_CASE("Graph test_R", "[Remove]")
{
    SECTION("Remove")
    {
        Graph<int> graph;
        graph.add_nodes(10);
        for (int i = 0; i < 10; ++i)
        {
            for (int j = 0; j < 10; ++j)
            {
                REQUIRE(graph.find_edge(i, j) == 0);
                graph.add_edge(i, j, i + j);
                REQUIRE(graph.find_edge(i, j) != 0);
            }
        }
        for (int i = 9; i >= 0; --i)
        {
            for (int j = 9; j >= 0; --j)
            {
                int Id = graph.find_edge(i, j);
                graph.remove_edge(Id);
                if (Id != 1)
                {
                    REQUIRE(graph.find_edge(i, j) == 0);
                }
            }
        }
    }
}

TEST_CASE("Greph test_A", "[Add]")
{
    SECTION("Add")
    {
        Graph<int> graph;
        for (int i = 0; i < 1000; ++i)
        {
            graph.add_node();
            REQUIRE((i + 1) == graph.get_size_node());
        }
    }
}

TEST_CASE("Graph test_D", "[Dijkstra]")
{
    SECTION("Serch")
    {
        Graph<int> graph;
        graph.add_nodes(5);
        graph.add_edge(0, 1, 9);
        graph.add_edge(1, 3, 5);
        graph.add_edge(0, 2, 2);
        graph.add_edge(0, 4, 1);
        graph.add_edge(1, 2, 3);
        graph.add_edge(2, 1, 3);
        graph.add_edge(2, 4, 6);
        graph.add_edge(2, 3, 9);
        graph.add_edge(4, 2, 6);
        graph.add_edge(4, 3, 7);
        auto res = graph.Dijkstra(0, 1);
        auto res_edges = res->get_path_ids();
        REQUIRE((*res_edges)[0] == 103);
        REQUIRE((*res_edges)[1] == 106);
        REQUIRE(res->get_path_weight() == 5);
        res = graph.Dijkstra(0, 3);
        res_edges = res->get_path_ids();
        REQUIRE((*res_edges)[0] == 104);
        REQUIRE((*res_edges)[1] == 110);
        REQUIRE(res->get_path_weight() == 8);
        res = graph.Dijkstra(1, 4);
        res_edges = res->get_path_ids();
        REQUIRE((*res_edges)[0] == 105);
        REQUIRE((*res_edges)[1] == 107);
        REQUIRE(res->get_path_weight() == 9);
    }
}

TEST_CASE("Graph test_P", "[Path]")
{
    SECTION("Union and crossing")
    {
        Graph<int> graph;
        graph.add_nodes(11);
        for (int i = 0; i < 10; ++i)
        {
            graph.add_edge(i, i + 1, 1);
        }
        Graph<int> graph_2;
        graph_2.add_nodes(11);
        for (int i = 0; i < 10; ++i)
        {
            graph_2.add_edge(i, i + 1, 1);
        }
        auto p_1 = graph.Dijkstra(6, 10);
        auto p_2 = graph.Dijkstra(0, 8);
        auto p_unite = p_1->crossing(p_2);
        auto p_cross = p_1->unite(p_2);
        auto p_un_id = p_unite->get_path_ids();
        auto p_cr_id = p_cross->get_path_ids();
        auto p_un_node = p_unite->get_path_nodes();
        auto p_cr_node = p_cross->get_path_nodes();
        for (int i = 0; i < 2; ++i)
        {
            REQUIRE((*p_un_id)[i] == 117 + i);
        }
        for (int i = 0; i < 3; ++i)
        {
            REQUIRE((*p_un_node)[i] == i + 6);
        }
        REQUIRE(p_unite->get_path_weight() == 2);
        for (int i = 0; i < 10; ++i)
        {
            REQUIRE((*p_cr_id)[i] == 111 + i);
        }
        for (int i = 0; i < 11; ++i)
        {
            REQUIRE((*p_cr_node)[i] == i);
        }
        REQUIRE(p_cross->get_path_weight() == 10);
        delete p_1;
        delete p_2;
        delete p_unite;
        delete p_cross;
        delete p_cr_id;
        delete p_cr_node;
        delete p_un_id;
        delete p_un_node;

        p_1 = graph.Dijkstra(6, 10);
        p_2 = graph_2.Dijkstra(0, 8);

        p_unite = p_1->crossing(p_2);
        p_cross = p_1->unite(p_2);

        p_un_id = p_unite->get_path_ids();
        p_cr_id = p_cross->get_path_ids();

        p_un_node = p_unite->get_path_nodes();
        p_cr_node = p_cross->get_path_nodes();

        REQUIRE(p_un_id->size() == 0);
        REQUIRE(p_un_node->size() == 0);
        REQUIRE(p_unite->get_path_weight() == 0);

        REQUIRE(p_cr_id->size() == 0);
        REQUIRE(p_cr_node->size() == 0);
        REQUIRE(p_cross->get_path_weight() == 0);

        delete p_1;
        delete p_2;
        delete p_unite;
        delete p_cross;
        delete p_cr_id;
        delete p_cr_node;
        delete p_un_id;
        delete p_un_node;

        p_1 = graph.Dijkstra(7, 10);
        p_2 = graph.Dijkstra(0, 6);

        p_unite = p_1->crossing(p_2);
        p_cross = p_1->unite(p_2);

        p_un_id = p_unite->get_path_ids();
        p_cr_id = p_cross->get_path_ids();

        p_un_node = p_unite->get_path_nodes();
        p_cr_node = p_cross->get_path_nodes();

        REQUIRE(p_un_id->size() == 0);
        REQUIRE(p_un_node->size() == 0);
        REQUIRE(p_unite->get_path_weight() == 0);

        REQUIRE(p_cr_id->size() == 0);
        REQUIRE(p_cr_node->size() == 0);
        REQUIRE(p_cross->get_path_weight() == 0);

        delete p_1;
        delete p_2;
        delete p_unite;
        delete p_cross;
        delete p_cr_id;
        delete p_cr_node;
        delete p_un_id;
        delete p_un_node;

        p_1 = graph.Dijkstra(7, 10);
        p_2 = graph.Dijkstra(0, 7);

        p_unite = p_1->crossing(p_2);
        p_cross = p_1->unite(p_2);

        p_un_id = p_unite->get_path_ids();
        p_cr_id = p_cross->get_path_ids();

        p_un_node = p_unite->get_path_nodes();
        p_cr_node = p_cross->get_path_nodes();

        REQUIRE(p_un_id->size() == 0);
        REQUIRE(p_un_node->size() == 0);
        REQUIRE(p_unite->get_path_weight() == 0);

        REQUIRE(p_cr_id->size() == 10);
        REQUIRE(p_cr_node->size() == 11);
        for (int i = 0; i < 10; ++i)
        {
            REQUIRE((*p_cr_id)[i] == 111 + i);
        }
        for (int i = 0; i < 11; ++i)
        {
            REQUIRE((*p_cr_node)[i] == i);
        }
        REQUIRE(p_cross->get_path_weight() == 10);

        delete p_1;
        delete p_2;
        delete p_unite;
        delete p_cross;
        delete p_cr_id;
        delete p_cr_node;
        delete p_un_id;
        delete p_un_node;
    }
}

TEST_CASE("Graph test_SCC", "[SCC]")
{
    SECTION("SCC")
    {
        Graph<int> graph;
        graph.add_nodes(5);
        graph.add_edge(0, 1, 3);
        graph.add_edge(1, 0, 4);
        graph.add_edge(1, 2, 9);
        graph.add_edge(2, 1, 7);
        graph.add_edge(3, 4, 8);
        graph.add_edge(4,3,4);
        graph.add_edge(0, 4, 5);
        auto res_scc = graph.strong_con_com();

        REQUIRE(res_scc[0][0] == 0);
        REQUIRE(res_scc[0][1] == 1);
        REQUIRE(res_scc[0][2] == 2);
        REQUIRE(res_scc[1][0] == 3);
        REQUIRE(res_scc[1][1] == 4);
    }
}

TEST_CASE("Graph topological sort", "[TOP_SORT]")
{
    SECTION("Top_sort")
    {
        Graph<int> graph_top;
        vector<int> res;
        graph_top.add_nodes(7);
        graph_top.add_edge(0, 3, 1);
        graph_top.add_edge(1, 3, 1);
        graph_top.add_edge(2, 3, 1);
        graph_top.add_edge(4, 3, 1);
        graph_top.add_edge(4, 1, 1);
        graph_top.add_edge(5, 2, 1);
        graph_top.add_edge(5, 4, 1);
        graph_top.add_edge(5, 3, 1);
        graph_top.add_edge(6, 4, 1);
        graph_top.add_edge(6, 2, 1);
        res = graph_top.topological_sort();
        REQUIRE(res[0] == 6);
        REQUIRE(res[1] == 5);
        REQUIRE(res[2] == 4);
        REQUIRE(res[3] == 2);
        REQUIRE(res[4] == 1);
        REQUIRE(res[5] == 0);
        REQUIRE(res[6] == 3);
    }
}

TEST_CASE("Graph reverse", "[Reverse]")
{
    SECTION("Reverse")
    {
        Graph<int> graph;
        graph.add_nodes(11);
        for (int i = 0; i < 10; ++i)
        {
            graph.add_edge(i, i + 1, 1);
        }
        auto res = graph.Dijkstra(0, 10);
        auto vector_1 = res->get_path_nodes();
        auto graph_r = graph.reversed_graph();
        auto res_r = graph_r.Dijkstra(10, 0);
        auto vector_2 = res_r->get_path_nodes();
        for (int i = 0; i < 11; ++i) {
            REQUIRE((*vector_1)[i] == (*vector_2)[10 - i]);
        }
        REQUIRE(res_r->get_path_weight() == res->get_path_weight());
    }
}


