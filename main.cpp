#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include "graph.hpp"
#include "Path.hpp"
using namespace std;

void graph_menu();

void graph_actions(int mode);

namespace menu
{
    int count = 0;
    void start_menu()
    {
        printf("\x1B[2J\x1B[H");
        printf("1) Make random graph\n");
        printf("2) Make clear graph\n");
        printf("3) Topological sort\n");
        printf("4) Spaning tree\n");
        printf("5) Tests\n");
        printf("6) Exit\n");
    }

    void actions()
    {
        printf("\x1B[2J\x1B[H");
        printf("1) Add node\n");
        printf("2) Add nodes\n");
        printf("3) Add edge\n");
        printf("4) Remove edge\n");
        printf("5) Search edge\n");
        printf("6) Dijkstra\n");
        printf("7) Dijkstra - second\n");
        printf("8) Unite path\n");
        printf("9) Crossing path\n");
        printf("10) Reversed graph\n");
        printf("11) Strong components\n");
        printf("12) Make picture\n");
        printf("13) Exit\n");
    }
}

int main()
{
    graph_menu();

    Graph<int> graph_top;
    vector<int> res;
    graph_top.add_nodes(7);
    graph_top.add_edge(0, 3, 1);
    graph_top.add_edge(1, 3, 1);
    graph_top.add_edge(2, 3, 1);
    graph_top.add_edge(4, 3, 1);
    graph_top.add_edge(4, 1, 1);
    graph_top.add_edge(5, 2, 1);
    graph_top.add_edge(5, 4, 1);
    graph_top.add_edge(5, 3, 1);
    graph_top.add_edge(6, 4, 1);
    graph_top.add_edge(6, 2, 1);
    res = graph_top.topological_sort();
    printf("Sorted Nodes: ");
    for (int re : res)
    {
        printf("%d ", re);
    }
    graph_top.Graphviz();
    return 0;
}


void graph_menu()
{
    Graph<int> graph_sp;
    Graph<int> graph_sp_2;
    graph_sp.add_nodes(7);
    graph_sp.add_edge(0, 1, 1);
    graph_sp.add_edge(1, 0, 1);
    graph_sp.add_edge(1, 3, 1);
    graph_sp.add_edge(3, 1, 1);
    graph_sp.add_edge(1, 2, 1);
    graph_sp.add_edge(2, 1, 1);
    graph_sp.add_edge(2, 3, 1);
    graph_sp.add_edge(3, 2, 1);
    graph_sp.add_edge(3, 4, 1);
    graph_sp.add_edge(4, 3, 1);
    graph_sp.add_edge(2, 4, 1);
    graph_sp.add_edge(4, 2, 1);

    vector<int> res;
    char c;
    int type;
    do
    {
       menu::start_menu();
       cin >> type;
       printf("\x1B[2J\x1B[H");
       switch (type)
       {
           case(1):
               graph_actions(1);
               break;
           case(2):
               graph_actions(0);
               break;
           case(3):
               break;
           case (4):
               graph_sp_2 = graph_sp.spanning_tree();
               graph_sp.Graphviz();
               system(((string)"mv Graph.png " + "Graph_sp.png").c_str());
               cout << "Graph in file: Graph_sp.png" << endl;
               graph_sp_2.Graphviz();
               system(((string)"mv Graph.png " + "Graph_sp_res.png").c_str());
               cout << "Graph in file: Graph_ap_res.png" << endl;
               sleep(2);
               break;
           case(5):
               system(((string) "g++ -std=c++17 "
                                "../CodingSandboxAllLingos/TestingWithCatch/*.cpp -o "
                                "res.exe")
                              .c_str());
               system(((string) "./res.exe").c_str());
               system(((string) "rm res.exe").c_str());
               read(0, &c, 1);
               break;
           case(6):
               break;
           default:
               printf("\x1B[2J\x1B[H");
               printf("Invalid input\n");
               sleep(1);
       }
    } while(type != 6 and type != 3);
}

void graph_actions(int mode)
{
    Path<int> *path;
    Path<int> *path_2;
    Path<int> *path_3;
    Graph<int> graph;
    Graph<int> reversed;
    vector<vector<int>> res_scc;
    vector<int> *res;
    int n, start, end, type;
    char c;
    system(((string)"rm -rf *.png").c_str());

    if (mode)
    {
        printf("\x1B[2J\x1B[H");
        printf("Input number of nodes and edges\n");
        int nodes, edges;
        cin >> nodes >> edges;
        graph.add_nodes(nodes);
        for (int i = 0; i < edges; ++i)
        {
            int st = 0, ed = 0;
            while (st == ed or graph.find_edge(st, ed))
            {
                st = (int)(rand() % nodes);
                ed = (int)(rand() % nodes);
            }
            graph.add_edge(st, ed, (int)(rand() + i) % (edges + 1) + 1);
        }
    }

    do {
        menu::actions();
        cin >> type;
        printf("\x1B[2J\x1B[H");
        switch (type)
        {
            case(1):
                graph.add_node();
                break;
            case(2):
                printf("Input number of elements\n");
                cin >> n;
                graph.add_nodes(n);
                break;
            case(3):
                printf("Input start\n");
                cin >> start;
                printf("Input end\n");
                cin >> end;
                printf("Input weight\n");
                cin >> n;
                graph.add_edge(start, end, n);
                break;
            case(4):
                printf("Input ID\n");
                cin >> n;
                graph.remove_edge(n);
                break;
            case (5):
                printf("Input start and end\n");
                cin >> start;
                cin >> end;
                n = graph.find_edge(start, end);
                if (n) {
                    printf("Id of edge: %d\n", n);
                } else {
                    printf("We do not have such edge :((\n");
                }
                sleep(2);
                break;
            case(6):
                printf("Input start and end\n");
                cin >> start >> end;
                path = graph.Dijkstra(start, end);
                res = path->get_path_ids();
                cout << "Id's: ";
                for (int re : *res) {
                    cout << re << " ";
                }
                delete res;
                res = path->get_path_nodes();
                cout << endl <<"Nodes: ";
                for (auto i: *res) {
                    cout << i << " ";
                }
                delete res;
                cout << endl <<"Length: " << path->get_path_weight() << endl;
                read(0, &c, 1);
                break;
            case(7):
                printf("Input start and end\n");
                cin >> start >> end;
                path_2 = graph.Dijkstra(start, end);
                res = path_2->get_path_ids();
                cout << "Id's: ";
                for (int re : *res) {
                    cout << re << " ";
                }
                delete res;
                res = path_2->get_path_nodes();
                cout << endl <<"Nodes: ";
                for (auto i: *res) {
                    cout << i << " ";
                }
                delete res;
                cout << endl <<"Length: " << path_2->get_path_weight() << endl;
                read(0, &c, 1);
                break;
            case (8):
                cout << "First path:" << endl;
                res = path->get_path_ids();
                cout << "Id's: ";
                for (int re : *res) {
                    cout << re << " ";
                }
                delete res;
                res = path->get_path_nodes();
                cout << endl <<"Nodes: ";
                for (auto i: *res) {
                    cout << i << " ";
                }
                delete res;
                cout << endl <<"Length: " << path->get_path_weight() << endl;
                cout << endl << "Second path:" << endl;
                res = path_2->get_path_ids();
                cout << "Id's: ";
                for (int re : *res) {
                    cout << re << " ";
                }
                delete res;
                res = path_2->get_path_nodes();
                cout << endl <<"Nodes: ";
                for (auto i: *res) {
                    cout << i << " ";
                }
                delete res;
                cout << endl <<"Length: " << path_2->get_path_weight() << endl;
                cout << endl << "United path: " << endl;
                path_3 = path->unite(path_2);
                res = path_3->get_path_ids();
                cout << "Id's: ";
                for (int re: *res) {
                    cout << re << " ";
                }
                delete res;
                res = path_3->get_path_nodes();
                cout << endl << "Nodes: ";
                for (auto i: *res) {
                    cout << i << " ";
                }
                delete res;
                cout << endl <<"Length: " << path_3->get_path_weight() << endl;
                read(0, &c, 1);
                break;
            case (9):
                cout << "First path:" << endl;
                res = path->get_path_ids();
                cout << "Id's: ";
                for (int re : *res) {
                    cout << re << " ";
                }
                delete res;
                res = path->get_path_nodes();
                cout << endl <<"Nodes: ";
                for (auto i: *res) {
                    cout << i << " ";
                }
                delete res;
                cout << endl <<"Length: " << path->get_path_weight() << endl;
                cout << endl << "Second path:" << endl;
                res = path_2->get_path_ids();
                cout << "Id's: ";
                for (int re : *res) {
                    cout << re << " ";
                }
                delete res;
                res = path_2->get_path_nodes();
                cout << endl <<"Nodes: ";
                for (auto i: *res) {
                    cout << i << " ";
                }
                delete res;
                cout << endl <<"Length: " << path_2->get_path_weight() << endl;
                cout << endl << "Crossed path: " << endl;
                path_3 = path->crossing(path_2);
                res = path_3->get_path_ids();
                cout << "Id's: ";
                for (int re: *res) {
                    cout << re << " ";
                }
                delete res;
                res = path_3->get_path_nodes();
                cout << endl << "Nodes: ";
                for (auto i: *res) {
                    cout << i << " ";
                }
                delete res;
                cout << endl <<"Length: " << path_3->get_path_weight() << endl;
                read(0, &c, 1);
                break;
            case(10):
                reversed = graph.reversed_graph();
                reversed.Graphviz();
                system(((string)"mv Graph.png " + "Graph_rev.png").c_str());
                cout << "Graph in file: Graph_rev.png" << endl;
                sleep(2);
                break;
            case (11):
                cout << "Strong connected components:\n";
                res_scc = graph.strong_con_com();
                for (int i = 0; i < res_scc.size(); ++i)
                {
                    printf("%d) ", i + 1);
                    for (int j : res_scc[i])
                    {
                        printf("%d ", j);
                    }
                    printf("\n");
                }
                read(0, &c, 1);
                break;
            case(12):
                graph.Graphviz();
                system(((string)"mv Graph.png " + "Graph_" + to_string(menu::count) + ".png").c_str());
                cout << "Graph in file: Graph_" <<  to_string(menu::count) << ".png" << endl;
                sleep(2);
                menu::count++;
                break;
            case(13):
                break;
            default:
                printf("Invalid input\n");
                sleep(1);
        }
    } while(type != 13);
}
